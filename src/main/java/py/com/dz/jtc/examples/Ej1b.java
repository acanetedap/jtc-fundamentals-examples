package py.com.dz.jtc.examples;

/**
 * Clase Ej1b
 * Curso de Programacion Java
 * @author Derlis Zarate 
 */
public class Ej1b {

    /**
     * Metodo main de la clase
     * @param args Argumentos de linea de comandos
     */
    public static void main(String[] args) {

        String saludo = "Hola ";
        String nombre = "NOMBRE";
        int edad = 25;
        String resultado = saludo + nombre;
        System.out.println(resultado);
        System.out.println(nombre + " tiene actualmente " + edad + " años");

    } //Fin de main

} //Fin de clase Ej1b
