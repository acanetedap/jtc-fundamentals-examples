package py.com.dz.jtc.examples;

import java.util.Scanner;

/**
 * Clase Ej4b 
 * Curso de Programacion Java
 * @author Derlis Zarate 
 */
public class Ej4b {

    /**
     * Metodo main de la clase
     * @param args Argumentos de linea de comandos
     */
    public static void main(String[] args) {

        int A, B;
        Scanner scan = new Scanner(System.in);

        System.out.print("Ingresa el primer nro: ");
        A = scan.nextInt();

        System.out.print("Ingresa el segundo nro: ");
        B = scan.nextInt();

        if (A != B) {
            int maximo = max(A, B);
            System.out.println("El mayor es " + maximo);
        } else {
            System.out.println("Los nros son iguales");
        }
        
        System.out.println("La suma es " + suma(A, B));
        
        System.out.println("La resta es " + resta(A, B));
        
        System.out.println("El producto es " + multiplicacion(A, B));
        
        if (B > 0) {
            System.out.println("El cociente entero es " + division(A, B));
        } else {
            System.out.println("No se puede dividir por cero");
        }

        scan.close();
    } //Fin de main

    public static int max(int A, int B) {
        return (A > B) ? A : B;
    }

    public static int suma(int A, int B) {
        return A + B;
    }

    public static int resta(int A, int B) {
        return A - B;
    }

    public static int multiplicacion(int A, int B) {
        return A * B;
    }

    public static int division(int A, int B) {
        return A / B;
    }
    
} //Fin de clase Ej4b
